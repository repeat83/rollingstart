func_res.getqualres = function(res)
	r_s.qualresult.filename = "qualresult.txt"

	local file
--	local qualfile = "qualresult.txt"
	-- ����� ������ ������ ���������� � ������ ����.
	if (res.reqi == 1) then
		local t = string.format("%02d %-24s %s\n",res.resultnum+1,res.uname,convert_btime(res.btime))
		file = assert(io.open("qualres"..r_s.qualresult.index..".txt", "a+"))
		file:write(t)
		file:close()
		return
	end

	if (res.resultnum ~= 255) then

		if (res.reqi == 0) then io.write("\n\tupdate "..res.resultnum.." res "..res.uname) end

		-- ����� ������ ����� ��������� ������������ �� ���� ������ ������ ���� �����������
		if (res.reqi == 0) then
			print("update collect res")
			luaLFS:tiny(2, TINY_RES)
			return
		end
		-- ��� ����������� ���������� ���������� � ����
		if (res.reqi ~= 0) then
			local t = string.format("%02d %-24s %s\n",res.resultnum+1,res.uname,convert_btime(res.btime))
			io.write("\n\t"..t)
			if (res.resultnum == 0) then file = assert(io.open(r_s.qualresult.filename, "w"))
			else file = assert(io.open(r_s.qualresult.filename, "a+"))
			end
			file:write(t)
			file:close()
			if (res.resultnum+1 == res.numres) then print("stop collect res") end
		end

	else
		io.write("\n\tno need update res of "..res.uname)
	end
end

function convert_btime(t)
	ms = string.sub(t,-3,-2)
	min = math.floor(string.sub(t,0,-4)/60)
	sec = string.sub(t,0,-4) - min*60
	return string.format("%d:%02d:%02d",min,sec,ms)
end